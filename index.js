import cors from 'cors'
import express from 'express'
import mongoose from 'mongoose'
import multer from 'multer'

import {
	loginValidator,
	postCreateValidator,
	registerValidator,
} from './validation.js'

import { PostController, UserController } from './controllers/index.js'
import { checkAuth, handleValidationErrors } from './utils/index.js'

mongoose
	.connect(
		'mongodb+srv://admin:wwwwww@cluster0.yqarbna.mongodb.net/blog?retryWrites=true&w=majority'
	)
	.then(() => console.log('mongoDB OK'))
	.catch(() => console.log('mongoDB isnt connected'))

const app = express()

const storage = multer.diskStorage({
	destination: (_, __, cb) => {
		cb(null, 'uploads')
	},
	filename: (_, file, cb) => {
		cb(null, file.originalname)
	},
})

const upload = multer({ storage })

app.use(express.json())
app.use(cors())
app.use('/uploads', express.static('uploads'))

app.post(
	'/auth/login',
	loginValidator,
	handleValidationErrors,
	UserController.login
)
app.post(
	'/auth/register',
	registerValidator,
	handleValidationErrors,
	UserController.register
)
app.get('/auth/me', checkAuth, UserController.getMe)

app.post('/upload', checkAuth, upload.single('image'), (req, res) => {
	res.json({
		url: `/uploads/${req.file.originalname}`,
	})
})

app.get('/tags', PostController.getLastTags)
app.get('/posts', PostController.getAll)
app.get('/posts/tags', PostController.getLastTags)
app.get('/posts/:id', PostController.getOne)
app.post(
	'/posts',
	checkAuth,
	postCreateValidator,
	handleValidationErrors,
	PostController.create
)
app.delete('/posts/:id', checkAuth, PostController.remove)
app.patch(
	'/posts/:id',
	checkAuth,
	postCreateValidator,
	handleValidationErrors,
	PostController.update
)

app.listen(4444, err => {
	if (err) {
		return console.log(err)
	}

	console.log('server ok')
})
